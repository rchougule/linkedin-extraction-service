import redis
import json
r = redis.Redis(host='localhost', port=6379, db=1)
p = r.pubsub()

def update_linkedin_status(email, status):
  r.hset(email, "linkedin", status)
  r.publish("linkedin", json.dumps({"email": email, "status": status}))