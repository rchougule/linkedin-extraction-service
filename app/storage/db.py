import pymongo
client = pymongo.MongoClient("localhost", 27017)
db = client.syndicate

def insert_linkedin_data(email, data):
  db.linkedin.insert_one(data)

def update_linkedin_status(email, status):
  db.status.update_one({"email": email}, {'$set': {"linkedin": status}})