from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# from ..storage import db
# from ..storage import redis_op
from queue import Queue
import time
import urllib.parse as url_parse
import threading
import re
from pathlib import Path
CHROMEDRIVER = Path.cwd().joinpath("chromedriver", "chromedriver")
LINKEDIN_EMAIL = []
LINKEDIN_PASS = []
total_tasks_queue = Queue(maxsize=50)
total_tasks_count = 0

def extract_interests_section(driver):
  interests = []
  time.sleep(1)
  interests_section = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(".pv-profile-section.pv-interests-section.artdeco-container-card.ember-view"))
  li_interests_section = interests_section.find_elements_by_tag_name("li")
  for li_interest in li_interests_section:
    print(li_interest.text)
    print("\n")

def extract_accom_section(driver):
  accomplishments = []
  time.sleep(1)
  try:
    accom_section = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(".pv-profile-section.pv-accomplishments-section.artdeco-container-card.ember-view"))
    sec_accom_section = accom_section.find_elements_by_tag_name("section")
    for sec_section in sec_accom_section:
      accom = {}
      first_sec = sec_section.find_elements_by_tag_name("h3")[0]
      first_div = sec_section.find_elements_by_tag_name("div")[0]
      first_sec_splits = first_sec.text.splitlines()
      first_div_splits = first_div.text.splitlines()
      if not len(first_div_splits) or not len(first_sec_splits):
        continue
      # accom[first_div_splits[0]] = first_div_splits[1]
      accom['type'] = first_div_splits[0]
      accom['data'] = first_div_splits[1]
      accom['count'] = first_sec_splits[1]
      accomplishments.append(accom)
  except Exception as e:
    print("Exception in Accomplishment Section : ", str(e))
  return(accomplishments)

def extract_skills_section(driver):
  skills = []
  driver.execute_script("window.scrollTo(0, 3240)")
  time.sleep(1)
  try:
    ol_skills_section = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(".pv-skill-categories-section__top-skills.pv-profile-section__section-info.section-info.pb1"))
    li_ol_skills_section = ol_skills_section.find_elements_by_tag_name("li")
    for li_skills_section in li_ol_skills_section:
      div_li_skills_section = li_skills_section.find_element_by_tag_name("div")
      div_skills_splits = div_li_skills_section.text.splitlines()
      if len(div_skills_splits):
        skills.append({"name": div_skills_splits[0], "count": div_skills_splits[1].split()[1]})
  except Exception as e:
    print("Exception in Skills Section : ", str(e))
  return(skills)

def extract_education_section(driver):
  education = []
  driver.execute_script("window.scrollTo(0, 2160)")
  try:
    education_section = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id("education-section"))
    ul_education_section = education_section.find_element_by_tag_name("ul")
    li_ul_education_section = ul_education_section.find_elements_by_tag_name("li")
    for li_education_section in li_ul_education_section:
      li_education_splits = li_education_section.text.splitlines()
      college = {}
      college['name'] = li_education_splits[0]
      i = 1
      while i < len(li_education_splits):
        if len(li_education_splits[i]) > 20 and (not (li_education_splits[i] == 'Dates attended or expected graduation')):
          activities = li_education_splits[i].split(":")
          if activities[0] == "Activities and Societies":
            college[activities[0]] = activities[1]
          else:
            college['Description'] = li_education_splits[i]
          i += 1
        else:
          if (i+1) >= len(li_education_splits):
            break
          college[li_education_splits[i]] = li_education_splits[i+1]
          i += 2
      education.append(college)
  except Exception as e:
    print("Exception in Education Section : ", str(e))
  return(education)

def extract_experience_section(driver):
  companies = []
  driver.execute_script("window.scrollTo(0, 1080)") 
  try:
    experience_section = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id("experience-section"))
    ul_experience_section = experience_section.find_element_by_tag_name("ul")
    li_ul_experience_section = ul_experience_section.find_elements_by_tag_name("li")
    for li_experience_section in li_ul_experience_section:
      company = {}
      titles = []
      # a_li_exp_sec = li_experience_section.find_element_by_css_selector('a')
      # print(a_li_exp_sec.text)
      # print(li_experience_section.text)
      companies_splits = li_experience_section.text.splitlines()
      
      if companies_splits[0] == 'Company Name':
        leng = (len(companies_splits))
        i = 0
        while i < leng:
          if companies_splits[i] != 'Title':
            if (i+1) >= len(companies_splits):
              break
            company[companies_splits[i]] = companies_splits[i+1]
            i += 2
          else:
            role = {}
            role['title'] = companies_splits[i+1]
            i += 2
            while True:
              if len(companies_splits[i]) > 20:
                if 'Description' in role:
                  role['Description'] = role['Description'] + companies_splits[i] + '\n'
                else:
                  role['Description'] = companies_splits[i] + '\n'
                i += 1
              else:
                if (i+1) >= len(companies_splits):
                  break
                role[companies_splits[i]] = companies_splits[i+1]
                i += 2
              if i >= len(companies_splits):
                break
              if companies_splits[i] == 'Title':
                break
            titles.append(role)
        company['titles'] = titles
        companies.append(company)
      else:
        if companies_splits[0] == 'Title':
          continue
        else:
          role = {}
          role['title'] = companies_splits[0]
          company[companies_splits[1]] = companies_splits[2]
          i = 3
          while True:
            if len(companies_splits[i]) > 20:
              if 'Description' in role:
                role['Description'] = role['Description'] + companies_splits[i] + '\n'
              else:
                role['Description'] = companies_splits[i] + '\n'
              i += 1
            else:
              if (i+1) >= len(companies_splits):
                break
              role[companies_splits[i]] = companies_splits[i+1]
              i += 2
            if i >= len(companies_splits):
              break
          company['titles'] = [role]
          companies.append(company)
  except Exception as e:
    print("Exception in Experience Section : ", str(e))    
  return(companies)

def get_profile_data(driver, profile_link):
  driver.get(profile_link)
  time.sleep(2)
  try:
    profile_photo_class = '.lazy-image.pv-top-card-section__photo.presence-entity__image.EntityPhoto-circle-9.loaded'
    profile_photo_link = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(profile_photo_class)).get_attribute('src')
    print(profile_photo_link)
    print("\n")
  except Exception as e:
    print("Exception in Profile Photo : ", str(e))

  try:
    tag_line_class = '.mt1.t-18.t-black.t-normal'
    tag_line = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(tag_line_class)).text
    print(tag_line)
    print("\n")
  except Exception as e:
    print("Exception in Tag Line : ", str(e))

  try:
    current_location_class = '.t-16.t-black.t-normal.inline-block'
    current_location = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(current_location_class)).text
    print(current_location)
    print("\n")
  except Exception as e:
    print("Exception in Current Location : ", str(e))

  about = ''
  try:
    classes = WebDriverWait(driver, 10).until(lambda driver: driver.find_elements_by_class_name("lt-line-clamp__ellipsis"))
    classes[0].find_element_by_css_selector('a').click()
    about_class = '.lt-line-clamp__raw-line'
    about = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(about_class)).text
  except Exception as e:
    print("Exception in About Data : ", str(e))
    try:
      about_spans = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_css_selector(".pv-about__summary-text.mt4.t-14.ember-view"))
      spans = about_spans.find_elements_by_tag_name("span")
      for span in spans:
        about = about + span.text + '\n'
    except Exception as e:
      print("Exception in About Data Retry : ", str(e))
  
  # print(about)
  companies = extract_experience_section(driver)
  # print(companies)

  education = extract_education_section(driver)
  # print(education)

  skills = extract_skills_section(driver)
  # print(skills)

  accomplishments = extract_accom_section(driver)
  # print(accomplishments)

  interests = extract_interests_section(driver)
  # print(interests)

  print("\n")

  time.sleep(20)
  # profile photo found.
  # scrape all the remaining data
  return(1)

def generate_google_search_link(basic_data):
  base_link = 'https://www.google.com/search?q=site%3Alinkedin.com'
  basic_data = [basic_data["name"], basic_data["company"]]
  for data in basic_data:
    for part in data.split():
      base_link += '+' + part
  print(base_link)
  return(base_link)

def search_profile(driver):
  result_xpath = '//*[@id="rso"]/div/div/div[1]/div/div'
  try:
    result_element = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath(result_xpath))
  except Exception as e:
    print("Error in Searching Profile Element : ", str(e))
    return(False)
  
  profile_link = result_element.find_element_by_css_selector('a').get_attribute('href')
  print(profile_link)
  return(profile_link)

def login(driver, master_email, master_password):
  driver.get('https://www.linkedin.com/login')
  # time.sleep(40)
  try:
    email_field = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id("username"))
    print("email : ", email_field.text)
    pass_field = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id("password"))
    print("pass : ", pass_field.text)
    login_button = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="app__container"]/main/div/form/div[3]/button'))
    print("login : ", login_button.text)

    email_field.clear()
    pass_field.clear()
    email_field.send_keys(master_email)
    pass_field.send_keys(master_password)
    login_button.click()
  except Exception as e:
    print("Error in Linkedin Login : ", str(e))


def start(basic_data):
  print(str(threading.current_thread().name) + " : Starting linkedin Fetch for : ", basic_data["email"])
  # redis_op.update_linkedin_status(basic_data["email"], "Running")
  # db.update_linkedin_status(basic_data["email"], "Running")
  # display = Display(visible=0, size=(800, 600))
  # display.start()
  options = Options()
  options.add_argument("--disable-notifications")

  options.add_argument("--window-size=1920,1080")
  options.add_argument("--disable-gpu")
  options.add_argument("--disable-extensions")
  # options.setExperimentalOption("useAutomationExtension", false);
  options.add_argument("--proxy-server='direct://'")
  options.add_argument("--proxy-bypass-list=*")
  options.add_argument("--start-maximized")
  # options.add_argument("--headless")

  capabilities = DesiredCapabilities.CHROME.copy()
  capabilities['acceptSslCerts'] = True 
  capabilities['acceptInsecureCerts'] = True
  # options.headless = True
  driver = webdriver.Chrome(CHROMEDRIVER, options=options, desired_capabilities=capabilities)
  search_link = generate_google_search_link(basic_data)
  driver.get(search_link)
  profile_link = search_profile(driver)
  if profile_link:
    login(driver, LINKEDIN_EMAIL[0], LINKEDIN_PASS[0])
    linkedin_data = get_profile_data(driver, profile_link)
  # time.sleep(1)

  # db.insert_linkedin_data(basic_data["email"], linkedin_data)
  # redis_op.update_linkedin_status(basic_data["email"], "Completed")
  # db.update_linkedin_status(basic_data["email"], "Completed")
  
  # cmore_sections_details = get_more_sections(driver, profile_link)
  
  driver.quit()
  global total_tasks_count
  total_tasks_count -= 1

# add the extraction task to queue
def add_extraction_task(basic_data):
  print(str(threading.current_thread().name) + " : Adding Extraction Task for : ", basic_data["email"])
  redis_op.update_linkedin_status(basic_data["email"], "Queued")
  db.update_linkedin_status(basic_data["email"], "Queued")
  
  total_tasks_queue.put(basic_data)

# begin the task operation with controlled thread pool
def start_queuing():
  print(str(threading.current_thread().name) + " : Queuing Started for linkedin Tasks...")
  global total_tasks_count
  global total_tasks_queue
  while True:
    total_popped = []
    if total_tasks_count < 5:
      while not total_tasks_queue.empty():
        total_popped.append(total_tasks_queue.get())
        if len(total_popped) == 5:
          break
      if len(total_popped):
        new_task_threads = [threading.Thread(target=start, args=(current_task_data,)) for current_task_data in total_popped]
        [new_task.start() for new_task in new_task_threads]
        total_tasks_count += len(total_popped)

# threading.Thread(target=start_queuing).start()

# start({"name":"akshay minocha", "company":"browserstack", "email":"aa@gmail.com"})
# start({"name":"rohan chougule", "company":"browserstack", "email":"aa@gmail.com"})
# start({"name":"divyum rastogi", "company":"browserstack", "email":"aa@gmail.com"})
# start({"name":"nishant gore", "company":"barclays", "email":"aa@gmail.com"})
